module.exports = function() {
  return new Promise(function(resolve,reject) {
    cordova.exec(function(result) {
      resolve(result);
    }, function(err) {
      reject(err);
    }, "THEMemoryStatus", "status", []);
  });
};

