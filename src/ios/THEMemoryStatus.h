#import <Cordova/CDVPlugin.h>

@interface THEMemoryStatus : CDVPlugin

- (void)status:(CDVInvokedUrlCommand*)command;

@end
