#import "THEMemoryStatus.h"
#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>
#import <mach/mach.h>
#import <mach/mach_host.h>

@implementation THEMemoryStatus

// from https://github.com/MobileChromeApps/cordova-plugin-chrome-apps-system-memory/blob/master/src/ios/ChromeSystemMemory.m

- (NSError *)kernelCallError:(NSString *)errMsg
{
    int code = errno;
	NSString *codeDescription = [NSString stringWithUTF8String:strerror(code)];

	NSDictionary* userInfo = @{
                              NSLocalizedDescriptionKey: [NSString stringWithFormat:@"%@: %d - %@", errMsg, code, codeDescription]
                              };
	
	return [NSError errorWithDomain:NSPOSIXErrorDomain code:code userInfo:userInfo];
}

- (NSNumber *)getAvailableMemory:(NSError **)error
{
    mach_port_t host_port;
    mach_msg_type_number_t host_size;
    vm_size_t pagesize;

    host_port = mach_host_self();
    host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    host_page_size(host_port, &pagesize);        

    vm_statistics_data_t vm_stat;

    if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS)
    {
		if (error)
		{
			*error = [self kernelCallError:@"Failed to fetch vm statistics"];
		}
        return nil;
    }
    
    /* Stats in bytes */ 
    natural_t mem_free = vm_stat.free_count * pagesize;
    
    return @(mem_free);
}

- (void)status:(CDVInvokedUrlCommand*)command
{
  CDVPluginResult* pluginResult = nil;

  // from https://stackoverflow.com/questions/787160/programmatically-retrieve-memory-usage-on-iphone:
  static unsigned last_resident_size=0;
  static unsigned greatest = 0;
  static unsigned last_greatest = 0;

  struct task_basic_info info;
  mach_msg_type_number_t size = sizeof(info);
  kern_return_t kerr = task_info(mach_task_self(),
                                 TASK_BASIC_INFO,
                                 (task_info_t)&info,
                                 &size);

  NSError* error = nil;

  NSNumber* capacity = [NSNumber numberWithUnsignedLongLong:[[NSProcessInfo processInfo] physicalMemory]];
  NSNumber* available = [self getAvailableMemory:&error];


  if( kerr != KERN_SUCCESS ) {
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                     messageAsString:[NSString stringWithFormat:@"Error with task_info(): %s",mach_error_string(kerr)]];

  } else if(error) {
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Could not get memory info"];
        
  } else {
    int diff = (int)info.resident_size - (int)last_resident_size;
    unsigned latest = info.resident_size;
    if( latest > greatest   )   greatest = latest;  // track greatest mem usage
    int greatest_diff = greatest - last_greatest;
    int latest_greatest_diff = latest - greatest;
    NSMutableDictionary * result = [NSMutableDictionary dictionaryWithCapacity:2];

    
    [result setObject:[NSNumber numberWithUnsignedLong:latest]
               forKey:@"resident_size"];

    [result setObject:[NSNumber numberWithUnsignedLong:greatest]
               forKey:@"max_resident_size"];

    [result setObject:available forKey:@"available"];
    [result setObject:capacity forKey:@"capacity"];
   
    NSProcessInfo * procInfo = [NSProcessInfo processInfo];
    

    last_resident_size = info.resident_size;
    last_greatest = greatest;

    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:result];
  }
  [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
